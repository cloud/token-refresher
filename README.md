# Token refresher

Refreshes OIDC tokens in kubernetes secrets.

Expects the keys `access_token` and `refresh_token` present in the secret.

Check the token status via kubernetes events.
```
$ kubectl get events --field-selector=involvedObject.name=token-secret
LAST SEEN   TYPE     REASON      OBJECT                MESSAGE
91s         Normal   Refreshed   secret/token-secret   Token was successfully refreshed
```

---

Based on [sample controller](https://github.com/kubernetes/sample-controller).
