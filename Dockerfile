FROM gcr.io/distroless/static

ADD token-refresher /token-refresher

ENTRYPOINT ["/token-refresher"]
