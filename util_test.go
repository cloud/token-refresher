package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestParseAccessToken(t *testing.T) {
	accessToken := "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJleGFtcGxlIiwiaXNzIjoiZXhhbXBsZS5jb20iLCJleHAiOjE2MDk0NTkyMDB9.cqbz0VCbHLtRzX_vEK7gTcfkCsKdCRTwFLrx1spK2ic"

	clientID, issuer, expiresAt, err := parseAccessToken(accessToken)
	require.NoError(t, err)

	require.Equal(t, "example", clientID)
	require.Equal(t, "example.com", issuer)
	require.Equal(t, time.Unix(1609459200, 0), expiresAt)
}
