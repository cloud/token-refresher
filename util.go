package main

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

// parseAccessToken decodes the claims in an OIDC access token to get the
// client ID, issuer URL and expiry time.
func parseAccessToken(accessToken string) (string, string, time.Time, error) {
	t, _, err := new(jwt.Parser).ParseUnverified(accessToken, &jwt.StandardClaims{})
	if err != nil {
		return "", "", time.Time{}, err
	}

	claims, ok := t.Claims.(*jwt.StandardClaims)
	if !ok {
		return "", "", time.Time{}, err
	}

	clientID := claims.Audience
	issuer := claims.Issuer
	expiresAt := time.Unix(claims.ExpiresAt, 0)

	return clientID, issuer, expiresAt, nil
}
