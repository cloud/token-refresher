DOCKER_REGISTRY ?= gitlab-registry.cern.ch/cloud/token-refresher
DOCKER_TAG ?= v0.0.1

.PHONY: test
test:
	go vet
	go test -v .

.PHONY: build
build:
	CGO_ENABLED=0 go build -o token-refresher -ldflags "-s -w" .

.PHONY: docker-build
docker-build: build
	docker build -t $(DOCKER_REGISTRY):$(DOCKER_TAG) .

.PHONY: docker-push
docker-push: docker-build
	docker push $(DOCKER_REGISTRY):$(DOCKER_TAG)
