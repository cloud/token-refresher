/*
Copyright 2021 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/oauth2"
	"golang.org/x/time/rate"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	corev1informers "k8s.io/client-go/informers/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	corev1listers "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
	"k8s.io/klog/v2"
)

const controllerAgentName = "token-refresher"

const (
	// Event reasons.
	Refreshed     = "Refreshed"
	RefreshFailed = "RefreshFailed"

	// Event messages.
	MessageRefreshed     = "Token was successfully refreshed"
	MessageRefreshFailed = "Token refresh failed: %v"
)

// Controller is the controller implementation for secrets
type Controller struct {
	// Cancellable context.
	ctx context.Context

	// kubeclientset is a standard kubernetes clientset
	kubeclientset kubernetes.Interface

	secretLister  corev1listers.SecretLister
	secretsSynced cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// NewController returns a new sample controller
func NewController(
	ctx context.Context,
	kubeclientset kubernetes.Interface,
	secretInformer corev1informers.SecretInformer) *Controller {

	// Create event broadcaster
	klog.V(4).Info("Creating event broadcaster")
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartStructuredLogging(0)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: kubeclientset.CoreV1().Events("")})
	recorder := eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	wq := workqueue.NewMaxOfRateLimiter(
		workqueue.NewItemExponentialFailureRateLimiter(5*time.Second, 100*time.Second),
		// 10 qps, 100 bucket size.  This is only for retry speed and its only the overall factor (not per item)
		&workqueue.BucketRateLimiter{Limiter: rate.NewLimiter(rate.Limit(10), 100)},
	)

	controller := &Controller{
		ctx:           ctx,
		kubeclientset: kubeclientset,
		secretLister:  secretInformer.Lister(),
		secretsSynced: secretInformer.Informer().HasSynced,
		workqueue:     workqueue.NewNamedRateLimitingQueue(wq, "Secrets"),
		recorder:      recorder,
	}

	klog.Info("Setting up event handlers")
	// Set up an event handler for when secrets change
	secretInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: controller.enqueueSecret,
		UpdateFunc: func(oldSecretObj, newSecretObj interface{}) {
			oldSecret := oldSecretObj.(*corev1.Secret)
			newSecret := newSecretObj.(*corev1.Secret)
			if newSecret.ResourceVersion == oldSecret.ResourceVersion {
				// No changes.
				return
			}
			controller.enqueueSecret(newSecretObj)
		},
	})

	return controller
}

// Run will set up the event handlers for types we are interested in, as well
// as syncing informer caches and starting workers. It will block until stopCh
// is closed, at which point it will shutdown the workqueue and wait for
// workers to finish processing their current work items.
func (c *Controller) Run(threadiness int, stopCh <-chan struct{}) error {
	defer utilruntime.HandleCrash()
	defer c.workqueue.ShutDown()

	// Start the informer factories to begin populating the informer caches
	klog.Info("Starting Secret controller")

	// Wait for the caches to be synced before starting workers
	klog.Info("Waiting for informer caches to sync")
	if ok := cache.WaitForCacheSync(stopCh, c.secretsSynced); !ok {
		return fmt.Errorf("failed to wait for caches to sync")
	}

	klog.Info("Starting workers")
	// Launch workers to process secrets
	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	klog.Info("Started workers")
	<-stopCh
	klog.Info("Shutting down workers")

	return nil
}

// runWorker is a long-running function that will continually call the
// processNextWorkItem function in order to read and process a message on the
// workqueue.
func (c *Controller) runWorker() {
	for c.processNextWorkItem() {
	}
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			utilruntime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		// Run processSecret, passing it the namespace/name string of the
		// Secret to be processed.
		if err := c.processSecret(key); err != nil {
			// Put the item back on the workqueue to handle any transient errors.
			c.workqueue.AddRateLimited(key)
			return fmt.Errorf("error syncing '%s': %s, requeuing", key, err.Error())
		}
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		klog.Infof("Successfully processed %q", key)
		return nil
	}(obj)

	if err != nil {
		utilruntime.HandleError(err)
		return true
	}

	return true
}

// processSecret attempts to refresh an OIDC token stored
// in a kubernetes secret.
//
// If an error occurs but processing should not be retried
// (e.g the secret no longer exists), then this logs an
// error message but returns nil instead of an error.
func (c *Controller) processSecret(key string) error {
	// Make sure this can't block forever on external connections (OIDC provider).
	ctx, cancelFunc := context.WithTimeout(c.ctx, 30*time.Second)
	defer cancelFunc()

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		utilruntime.HandleError(fmt.Errorf("invalid resource key: %s", key))
		return nil
	}

	secretCache, err := c.secretLister.Secrets(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			utilruntime.HandleError(fmt.Errorf("secret %q in work queue no longer exists", key))
			return nil
		}

		return err
	}

	secret := secretCache.DeepCopy()

	var accessTokenBytes, refreshTokenBytes []byte
	var ok bool
	if accessTokenBytes, ok = secret.Data["access_token"]; !ok {
		klog.Infof("Ignoring %q, does not have access_token field", key)
		return nil
	}
	if refreshTokenBytes, ok = secret.Data["refresh_token"]; !ok {
		klog.Infof("Ignoring %q, does not have refresh_token_key field", key)
		return nil
	}

	accessToken := string(accessTokenBytes)
	refreshToken := string(refreshTokenBytes)

	// If the secret was created from files then the data
	// may have a newline at the end, so remove it.
	accessToken = strings.TrimRight(accessToken, "\n")
	refreshToken = strings.TrimRight(refreshToken, "\n")

	// Extract necessary information from the access token.
	clientID, issuer, expiresAt, err := parseAccessToken(accessToken)
	if err != nil {
		utilruntime.HandleError(fmt.Errorf("failed to parse access token: %v", err))
		return nil
	}

	// If the token isn't close to needing refreshing,
	// requeue it at a later time.
	if time.Until(expiresAt) > 2*time.Minute {
		delay := time.Until(expiresAt) - time.Minute
		klog.Infof("Requeueing %q in %v", key, delay.Round(time.Second))
		c.workqueue.AddAfter(key, delay)
		return nil
	}

	// Create an OIDC provider for the URL stored in the access token.
	provider, err := oidc.NewProvider(ctx, issuer)
	if err != nil {
		err = fmt.Errorf("failed to create OIDC provider: %v", err)
		utilruntime.HandleError(err)
		c.recorder.Eventf(secret, corev1.EventTypeNormal, RefreshFailed, MessageRefreshFailed, err)
		return err
	}

	oauth2Config := oauth2.Config{
		ClientID: clientID,
		Endpoint: provider.Endpoint(),
	}

	// Set the expiration time in the past to force it to refresh.
	currentToken := &oauth2.Token{
		Expiry:       time.Now().Add(-time.Minute),
		RefreshToken: refreshToken,
	}

	// Get a new (refreshed) token.
	source := oauth2Config.TokenSource(ctx, currentToken)
	token, err := source.Token()
	if err != nil {
		utilruntime.HandleError(fmt.Errorf("failed to refresh token: %v", err))
		c.recorder.Eventf(secret, corev1.EventTypeNormal, RefreshFailed, MessageRefreshFailed, err)
		return err
	}

	// Update the data in the secret.
	secret.Data["access_token"] = []byte(token.AccessToken)
	secret.Data["refresh_token"] = []byte(token.RefreshToken)
	_, err = c.kubeclientset.CoreV1().Secrets(namespace).Update(ctx, secret, metav1.UpdateOptions{})
	if err != nil {
		utilruntime.HandleError(fmt.Errorf("failed to update secret"))
		c.recorder.Eventf(secret, corev1.EventTypeNormal, RefreshFailed, MessageRefreshFailed, "failed to update secret")
		return err
	}

	c.recorder.Event(secret, corev1.EventTypeNormal, Refreshed, MessageRefreshed)

	// Requeue the secret shortly before it next expires.
	_, _, nextExpiresAt, err := parseAccessToken(token.AccessToken)
	if err != nil {
		utilruntime.HandleError(fmt.Errorf("failed to parse next access token expiry time: %v", err))
		return err
	}
	delay := time.Until(nextExpiresAt) - time.Minute
	klog.Infof("Refreshed %q, requeueing in %v", key, delay.Round(time.Second))
	c.workqueue.AddAfter(key, delay)

	return nil
}

// enqueueSecret takes a Secret and puts the namespace/name into the work queue
// if it looks like it contains an OIDC access/refresh token.
func (c *Controller) enqueueSecret(obj interface{}) {
	var key string
	var err error
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		utilruntime.HandleError(err)
		return
	}
	secret, ok := obj.(*corev1.Secret)
	if !ok {
		utilruntime.HandleError(fmt.Errorf("failed to convert obj for %q to secret type", key))
		return
	}
	_, hasAccess := secret.Data["access_token"]
	_, hasRefresh := secret.Data["refresh_token"]
	if hasAccess && hasRefresh {
		c.workqueue.Add(key)
	}
}
